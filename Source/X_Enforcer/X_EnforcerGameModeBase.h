// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "X_EnforcerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class X_ENFORCER_API AX_EnforcerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
