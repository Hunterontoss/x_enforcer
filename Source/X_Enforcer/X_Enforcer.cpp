// Copyright Epic Games, Inc. All Rights Reserved.

#include "X_Enforcer.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, X_Enforcer, "X_Enforcer" );
